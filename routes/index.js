var express = require('express');
var router = express.Router();
var log = require('../utils/logger');
var matcher = require('../utils/matcher');


/* GET home page. */
router.get('/', function(req, res) {
  if(!req.session.passport.user) {
    // call endpoint for login page.
    res.render('login');
    return;
  }
  res.render('presses');
  return;
});

module.exports = router;
