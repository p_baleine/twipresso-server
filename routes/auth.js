var express = require('express');
var router = express.Router();
var passport = require('passport');

/* POST Twitter oAuth. */
router.get('/', passport.authenticate('twitter'), function(req, res){
  // The request will be redirected to Twitter for authentication, so this
  // function will not be called.
  console.log(res);
});

router.get('/callback', passport.authenticate('twitter', { failureRedirect: '/login' }),
  function(req, res) {
    res.redirect('/');
  });

module.exports = router;
