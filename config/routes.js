var app = require('../app');

var routes = require('../routes/index');
app.use('/', routes);

var auth = require('../routes/auth');
app.use('/api/auth', auth);

var logout = require('../routes/logout');
app.use('/api/logout', logout);

var press = require('../routes/press');
app.use('/api/press', press);
