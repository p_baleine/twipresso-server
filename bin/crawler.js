#!/usr/bin/env node

var request = require('request'),
    jsdom = require('jsdom'),
    Twit = require('twit'),
    iconv = require('iconv');

var log = require('../utils/logger');
var matcher = require('../utils/matcher');

// db settings
require('../config/db');
var mongoose = require('mongoose');
var Press = mongoose.model('Press');
var User = mongoose.model('User');

// constants
var jquerySrc = 'http://code.jquery.com/jquery-2.1.1.js';


var correctTweetContainsUri = function(userId, lastCreatedAt, twit, done) {
  twit.get('statuses/home_timeline', { count: 100 }, function(err, data, response) {
    if(err) {
      log('Failed to get tweets.', err);
      return;
    }
    var presses = [];
    for(var i = 0 ; i < data.length ; i++) {
      var item = data[i];
      var uri = matcher.url.first(item.text);
      if(!uri) {
        continue;
      }
      var time = Number(new Date(item.created_at));

      if(lastCreatedAt >= time) {
        continue;
      }
      var press = new Press({
        userId: userId,
        screen_name: item.user.screen_name,
        user_name: item.user.name,
        profile_image_uri: item.user.profile_image_url,
        created_at: time,
        text: item.text,
        link: {
          url: uri
        }
      });
      presses.push(press);
    }
    log('Target data count: [' + presses.length + ']');
    done(presses);
  });
};

var convertCharset = function(response, buf) {
  var charset = null;
  var content_type = response.headers['content-type'];
  if (content_type) {
    var re = content_type.match(/\bcharset=([\w\-]+)\b/i);
    if (re) {
      charset = re[1];
    }
  }

  if (!charset) {
    var bin = buf.toString('binary');
    re = bin.match(/<meta\b[^>]*charset=([\w\-]+)/i);
    if (re) {
      charset = re[1];
    } else {
      charset = 'utf-8';
    }
  }

  switch (charset) {
  case 'ascii':
  case 'utf-8':
    return buf.toString(charset);
    break;

  default:
    var ic = new (iconv.Iconv)(charset, 'utf-8');
    var buf2 = ic.convert(buf);
    return buf2.toString('utf8');
    break;
  }
};

var fetchExtractContent = function(presses, index, done) {

  log('extracting : ' + index);
  if(index >= presses.length) {
    done();
    return;
  }

  var press = presses[index];
  var uri = press.link.url;

  request({uri:uri, timeout: 2000}, function (err, res, body) {
    if (err) {
      log('Failed to connect link.', err);
      fetchExtractContent(presses, ++index, done);
      return;
    } else if (res.statusCode != 200) {
      log('Failed to get HTML content.', res.statusCode);
      fetchExtractContent(presses, ++index, done);
      return;
    } else if(!body || body.length == 0) {
      log('Body is empty.');
      fetchExtractContent(presses, ++index, done);
      return;
    }

    try {
      log('Converting charset start.');
      body = convertCharset(res, body);
      log('Converting charset completed.');
    } catch(e) {
      log('Failed to convert charset.', e);
      fetchExtractContent(presses, ++index, done);
      return;
    }

    var options = {
      features:{
        FetchExternalResources: false,
        ProcessExternalResources: false
      }
    };
    var window = jsdom.jsdom(body, options).parentWindow;
    jsdom.jQueryify(window, jquerySrc, function (window) {
      log('jQueryify success. [' + press.link.url + ']');
      press.link.title = window.$('title').text();
      press.link.content = window.$('p').text().substr(0, 100).replace(/\s+/g, " ");

      var img = window.$("main img");
      if(img.length < 1) {
        img = window.$("#main img");
      }
      if(img.length < 1) {
        img = window.$(".main img");
      }
      if(img.length < 1) {
        img = window.$("[role='main'] .permalink-footer  img");
      }
      if(img.length < 1) {
        img = window.$('img');
      }

      img.each(function() {
        if(this.src) {
          if(this.src.indexOf('http') > -1) {
            press.link.image_url = this.src;
            return false;
          }
        }
        return true;
      });
      log('insert data start. data=[' + JSON.stringify(press) + ']');
      insertData(press, function() {
        log('insert data completed.');
        fetchExtractContent(presses, ++index, done);
      });
    });
  });
};

var insertData = function(press, done) {
  press.save(function(err, doc) {
    if(err) {
      log('Failed to insert data to db.', err);
    }
    done();
  });
};

var closeConnection = function() {
  log('Insert data completed.');
  mongoose.disconnect();
};

// Twitter configuration.
try {
  var twitter = require('../config/secret').twitter;
} catch(e) {
  twitter = 
    {
      consumerKey: process.env.CONSUMER_KEY,
      consumerSecret: process.env.CONSUMER_SECRET
    };
}


var fetchUsers = function(users, userIndex, done) {
  log('Fetch users. index[' + userIndex + ']');
  if(userIndex >= users.length) {
    done();
    return;
  }

  var user = users[userIndex];
  var userId = user.twitterId;
  log('Crawl start. user: [' + userId + ']');
  Press.find({userId: userId}, 'created_at', {limit: 1, sort: {created_at: -1}}, function(err, docs) {
    var lastCreatedAt = 0;
    if(docs[0]) {
      lastCreatedAt = docs[0].created_at;
    }

    var twit = new Twit({
      consumer_key: twitter.consumerKey,
      consumer_secret: twitter.consumerSecret,
      access_token: user.accessToken,
      access_token_secret: user.accessTokenSecret
    });
    var res = correctTweetContainsUri(userId, lastCreatedAt, twit, function(presses) {
      fetchExtractContent(presses, 0, function() {
        fetchUsers(users, ++userIndex, done);
      });
    });
  });
};

User.find({}, function(err, users) {
  if(err) {
    log('Failed to retrieve user data.');
    return;
  }
  fetchUsers(users, 0, function() {
    closeConnection();
    return;
  });
});

